from random import random
from maya import OpenMaya as om

sel = om.MSelectionList()
sel.add('*')

itSel = om.MItSelectionList(sel, om.MFn.kMesh)
itDag = om.MItDag()
while not itDag.isDone():
    try:
        mesh = om.MFnMesh(itDag.currentItem())
        
        print ' *', mesh.name()
        
        itFaces = om.MItMeshPolygon(itDag.currentItem())
        while not itFaces.isDone():
            a = random()
            # print '  -', itFaces.index(), '/', itFaces.count()
            
            c = itFaces.center()
            mesh.setFaceColor(om.MColor(c.x, c.y, c.z, a), itFaces.index())
            
            itFaces.next()
        
    except Exception as e:
        pass
        
    itDag.next()