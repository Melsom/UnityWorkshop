try:
    import maya.standalone
    maya.standalone.initialize()
    print 'maya inited:', __file__
except Exception as e:
    print e

import random, os
from pymel.core import *
from maya import mel

def doFile(path):
    print 'doing file:', path
    newFile(force=1)
    importFile(path)
    
    print 'triangulating and detaching...'
    meshes = [shape for shape in ls(s=1) if isinstance(shape, nt.Mesh)]
    for mesh in meshes:
        select(mesh)
        mel.eval('Triangulate')
        select(mesh.vtx)
        mel.eval('DetachComponent')
    print 'done!'

    # done in engine instead
    # print 'painting vertices...'
    # execfile(os.path.join(__file__, '../openmaya.py'))
    # print 'done!'

    select([])
    for mesh in meshes:
        select(mesh, add=True)

    out = os.path.splitext(path)[0] + '.fbx'
    exportSelected(out, force=1)
    print 'done with:', path

def doFolder(path):
    for root, dirs, files in os.walk(path):
        for f in files:
            if '.fbx' not in os.path.splitext(f)[1]:
                continue
            
            print 'doing', f
            
            doFile(root + '/' + f)

doFolder(r'C:\Users\Garm\Documents\Unity Projects\UnityWorkshop\Assets\SimpleCity\_Models')