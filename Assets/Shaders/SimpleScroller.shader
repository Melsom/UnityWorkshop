﻿Shader "Tar Valley/Simple Scroller"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color Multiply", Color) = (1.0, 1.0, 1.0, 1.0)
		_Ambient ("Color Add", Color) = (0.0, 0.0, 0.0, 1.0)
		_SpeedX ("Scroll Speed X", Float) = 1
		_SpeedY ("Scroll Speed Y", Float) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _Color;
			float4 _Ambient;
			float _TileX;
			float _TileY;
			float _SpeedX;
			float _SpeedY;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				float2 uv = i.uv;
				uv.x += _Time * _SpeedX;
				uv.y += _Time * _SpeedY;
				fixed4 col = tex2D(_MainTex, frac(uv));
				col *= _Color;
				col += _Ambient;
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
