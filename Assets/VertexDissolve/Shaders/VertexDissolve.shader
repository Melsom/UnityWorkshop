﻿Shader "Tar Valley/Vertex Dissolve"
{
	Properties
	{
        _Color("Color", Color) = (1,1,1,1)
		_MainTex ("Texture", 2D) = "white" {}
		_GlowColor ("Glow Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_GlowAmount ("Glow Amount", Float) = 1
		_GlowFalloff ("Glow Fallof", Float) = 1
		_GlowFalloffOffset ("Glow Fallof Offset", Float) = 0
        _Progress ("Progress", Range(0, 1)) = 0.5
        _NormalWeight ("Normal Weight", Float) = 1.0
        _DistanceDirection ("Distance Direction", Vector) = (0.0, 1.0, 0.0, 0.0)
        _DistanceWeight ("Distance Weight", Float) = 1.0
		_WaveSize ("Wave Size", Float) = 1.0
		_Angle ("Angle", Float) = 90.0
        _Cutoff ("Alpha cutoff", Range (0,1)) = 0.5
	}
	SubShader
	{
		Tags { "Queue"="AlphaTest" "RenderType"="TransparentCutout" "IgnoreProjector"="True" "LightMode"="ForwardBase" }
		LOD 100

		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha
		AlphaTest LEqual [_Cutoff]
		
		Pass
		{	
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float4 normal : NORMAL;
				float4 color : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR0;
			};

			float4 _Color;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _GlowColor;
			float _GlowAmount;
			float _GlowFalloff;
			float _GlowFalloffOffset;
			float _Progress;
            float _NormalWeight;
            float4 _DistanceDirection;
            float _DistanceWeight;
			float _WaveSize;
			float _Angle;
			float _Cutoff;
			
			#include "VertexDissolveCore.cginc"

			v2f vert (appdata v)
			{
				OutVD vd = VertexDissolveMesh(v, _Angle, _Progress);

				float3 N = UnityObjectToWorldNormal(v.normal);
				float Ndotl = saturate(dot(N, _WorldSpaceLightPos0.xyz));

				v2f o;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.vertex = UnityObjectToClipPos(vd.position);
				o.color = float4(Ndotl * _LightColor0.xyz, vd.progress);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float a = VertexDissolveGlow(i.color.w, _GlowFalloff, _GlowFalloffOffset);

				fixed4 outColor = tex2D(_MainTex, i.uv) * _Color;
				outColor *= float4(i.color.rgb, 1.0);
				outColor += _GlowColor * _GlowAmount * a * _GlowColor.a;

				clip((1 - i.color.w) - _Cutoff);
				return float4(outColor.rgb, 1.0);
			}
			ENDCG
		}
	}
}
