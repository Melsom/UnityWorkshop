struct InVD {
    float4 position;
    float2 uv;
    float2 uv2;
    float4 normal;
    float4 color;
};

struct OutVD {
	float3 position;
	float progress;
};

static const float PI = 3.14159265;

inline OutVD VertexDissolve(float3 vPos, float3 fPos, float3 tPos, float tAngle, float fValue, float progress) {
	float alpha = lerp(0, 1, fValue - (progress * 2) + 1) * 4 - 2;
    float alphaSat = saturate(alpha);

    // rotation
    // subtract facePos to bring face to origin
	float3 outPos = vPos - fPos;
    // rotate x
    float a = lerp(0, tAngle * 180/PI * fValue, alphaSat);
	float4x4 rx = {
		{1, 0, 0, 0},
		{0, cos(a), -sin(a), 0},
		{0, sin(a), cos(a), 0},
		{0, 0, 0, 1}
	};
	outPos = mul(rx, outPos);
    // rotate y
	float4x4 ry = {
		{cos(a), 0, sin(a), 0},
		{0, 1, 0, 0},
		{-sin(a), 0, cos(a), 0},
		{0, 0, 0, 1}
	};
	outPos = mul(ry, outPos);
    // rotate z
	float4x4 rz = {
		{cos(a), -sin(a), 0, 0},
		{sin(a), cos(a), 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1}
	};
	outPos = mul(rz, outPos);
    // add facePos to bring face back to object space
    outPos += fPos;
				
	// scale
    outPos = lerp(outPos, fPos, alphaSat);
	// translate
    outPos = lerp(outPos, outPos + tPos, alphaSat);
	outPos.x += sin(outPos.y * _WaveSize + fValue + outPos.x) * alphaSat;
	outPos.z += sin(outPos.y * _WaveSize + fValue + outPos.z) * alphaSat;

    OutVD OUT;
    OUT.position = outPos;
    OUT.progress = alpha;
    return OUT;
}

inline OutVD VertexDissolveMesh(InVD IN, float tAngle, float progress) {
    float3 tPos = IN.normal * _NormalWeight + _DistanceDirection * _DistanceWeight;
    float fValue = IN.uv2.y * (IN.color.w * 0.5 + 0.5);
    return VertexDissolve(IN.position, IN.color.rgb, tPos, tAngle, fValue, progress);
}

inline float VertexDissolveGlow(float progress, float falloff, float offset) {
    return pow(clamp(progress + offset, 0, 1), falloff);
}