﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TarValley.VertexDissolve
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(MeshFilter))]
    public class Colorizer : MonoBehaviour
    {
        MeshFilter _meshFilter;
        public MeshFilter meshFilter
        {
            get
            {
                if (!_meshFilter)
                {
                    _meshFilter = GetComponent<MeshFilter>();
                }
                return _meshFilter;
            }
        }

        void Start()
        {
            Mesh mesh = meshFilter.sharedMesh;
            List<Color> cols = new List<Color>();
            for (int i = 0; i < mesh.triangles.Length / 3; i++)
            {
                Vector3 p = Vector3.zero;
                for (int j = 0; j < 3; j++)
                {
                    int v = mesh.triangles[i * 3 + j];
                    p += mesh.vertices[v];
                }
                p /= 3;
                float r = Random.Range(0.0f, 1.0f);
                for (int j = 0; j < 3; j++)
                {
                    int v = mesh.triangles[i * 3 + j];
                    cols.Add(new Color(p.x, p.y, p.z, r));
                }
            }
            mesh.SetColors(cols);
        }
    }
}