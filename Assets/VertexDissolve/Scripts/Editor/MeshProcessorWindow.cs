﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace TarValley.VertexDissolve
{
    public class MeshProcessorWindow : EditorWindow
    {
        float labelWidth = 75;

        bool splitVerts = true;
        bool colorize = true;

        int selectedMode;
        string path;
        Mesh mesh;

        [MenuItem("Tar Valley/Vertex Dissolve")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow<MeshProcessorWindow>(
                true,
                "Mesh Processing"
            );
        }

        void OnGUI()
        {
            GUILayout.Label("Settings", EditorStyles.boldLabel);

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Split Verts?", GUILayout.Width(labelWidth));
            splitVerts = EditorGUILayout.Toggle(splitVerts);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Colorize?", GUILayout.Width(labelWidth));
            colorize = EditorGUILayout.Toggle(colorize);
            EditorGUILayout.EndHorizontal();


            GUILayout.Label("Target(s)", EditorStyles.boldLabel);

            selectedMode = GUILayout.Toolbar(selectedMode, new string[] { "Single", "Batch" });

            EditorGUILayout.BeginHorizontal();
            switch (selectedMode)
            {
                case 0:
                    GUILayout.Label("File", GUILayout.Width(labelWidth));
                    mesh = (Mesh)EditorGUILayout.ObjectField(mesh, typeof(Mesh), true);
                    break;
                case 1:
                    GUILayout.Label("Folder", GUILayout.Width(labelWidth));
                    path = EditorGUILayout.TextField(path);
                    if (GUILayout.Button("Browse"))
                    {
                        path = EditorUtility.OpenFolderPanel("Open Folder", Application.dataPath, "fbx");
                        path = path.Replace(Application.dataPath, "Assets");
                    }
                    break;
            }
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Start Processing"))
            {
                switch (selectedMode)
                {
                    case 0: ProcessMesh(mesh); break;
                    case 1: ProcessFolder(path); break;
                }
            }
        }

        void ProcessFolder(string path)
        {
            string[] files = Directory.GetFiles(path);
            List<Mesh> meshes = new List<Mesh>();
            foreach (string file in files)
            {
                Mesh mesh = (Mesh)AssetDatabase.LoadAssetAtPath(file, typeof(Mesh));
                if (mesh)
                {
                    meshes.Add(mesh);
                }
            }
            
            Mesh[] array = meshes.ToArray();
            for (int i = 0; i < array.Length; i++)
            {
                EditorUtility.DisplayProgressBar("Processing Folder...", "", ((float)i)/array.Length);
                ProcessMesh(array[i]);
            }
            EditorUtility.ClearProgressBar();
        }

        void ProcessMesh(Mesh mesh) 
        {
            if (splitVerts)
            {
                MeshProcessor.HardenEdges(mesh);
            }
            if (colorize) 
            {
                MeshProcessor.ColorVertices(mesh);
            }
        }
    }
}
