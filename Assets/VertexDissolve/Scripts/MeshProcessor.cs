using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TarValley.VertexDissolve
{
    public class MeshProcessor
    {
        public static void ColorVertices(Mesh mesh)
        {
            float largest = 0.0f;
            float smallest = 999999.0f;
            List<Vector3> pos = new List<Vector3>();
            for (int i = 0; i < mesh.triangles.Length / 3; i++)
            {
                Vector3 p = Vector3.zero;
                for (int j = 0; j < 3; j++)
                {
                    int v = mesh.triangles[i * 3 + j];
                    p += mesh.vertices[v];
                }
                p /= 3;
                pos.Add(p);
                if (p.y > largest)
                {
                    largest = p.y;
                }
                if (p.y < smallest)
                {
                    smallest = p.y;
                }
            }

            List<Color> cols = new List<Color>();
            List<Vector2> uv2s = new List<Vector2>();
            for (int i = 0; i < pos.Count; i++)
            {
                Vector3 p = pos[i];
                float r = Random.Range(0.0f, 1.0f);
                for (int j = 0; j < 3; j++)
                {
                    cols.Add(new Color(p.x, p.y, p.z, r));
                    uv2s.Add(new Vector2(0, (p.y + Mathf.Abs(smallest)) / (Mathf.Abs(smallest) + Mathf.Abs(largest))));
                }
            }
            mesh.SetColors(cols);
            mesh.SetUVs(1, uv2s);
        }

        public static void HardenEdges(Mesh mesh)
        {
            List<Vector3> vertices = new List<Vector3>();
            List<Vector2> uv = new List<Vector2>();
            List<int> triangles = new List<int>();
            for (int i = 0; i < mesh.triangles.Length; i++) 
            {
                int j = mesh.triangles[i];
                Vector3 v = mesh.vertices[j];
                Vector2 t = mesh.uv[j];

                vertices.Add(v);
                uv.Add(t);
                triangles.Add(i);
            }
            mesh.SetVertices(vertices);
            mesh.SetUVs(0, uv);

            for (int i = 0; i < mesh.subMeshCount; i++)
            {
                List<int> newTris = new List<int>();
                for (int j = (int)mesh.GetIndexStart(i); j < mesh.GetIndexStart(i) + mesh.GetIndexCount(i); j++)
                {
                    newTris.Add(triangles[j]);
                }

                mesh.SetTriangles(newTris.ToArray(), i);
            }

            mesh.RecalculateNormals();
        }
    }
}